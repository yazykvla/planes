% Determine where your m-file's folder is.
folder = fileparts(which(mfilename)); 
% Add that folder plus all subfolders to the path.
addpath(genpath(folder));
clear folder
Airframe;
Aerodynamic;
Powertrain;
AerodynamicSurfaces;
Initial;
Trim;
Path; % Initialize the path
ProcessPath; % Process the path to get time points and heading