%% Remove duplicate points
points = [x; y; z; v];
tol = 1e-10;
duplicates = [zeros(4, 1), abs(points(:, 2:end) - points(:, 1:end-1)) < tol];

duplicates_position = duplicates(1, :) & duplicates(2, :) & duplicates(3, :);
duplicates_velocity = duplicates(4, :);

if sum(sum(duplicates_position & (~duplicates_velocity))) > 0
    error("Duplicates in position but not in velocity!")
end

points = points(:, ~(duplicates_position & duplicates_velocity));

x = points(1, :);
y = points(2, :);
z = points(3, :);
v = points(4, :);

positions = [x; y; z];

%% Differences between them
d_positions = positions(:, 2:end) - positions(:, 1:end-1);

%% Distances = norms of the differences
distances = vecnorm(d_positions);

%% Calculate time points
v_avg = (v(2:end) + v(1:end - 1)) / 2;
t = cumsum([0, distances ./ v_avg]);

%% Calculade the headings
dx = d_positions(1, :);
dy = d_positions(2, :);
heading = atan2(dy, dx);
heading = [heading(1), heading];

dheading = [0, heading(2:end) - heading(1:end-1)];

% Remove the discontinuity at Pi
jumps = abs(dheading) > pi;
jumps = sign(dheading) .* jumps;
lifts = jumps * -2 * pi;
lifts = cumsum(lifts);
heading = heading + lifts;