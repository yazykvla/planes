%% Parameters
maxSpeed = 90;
turnRadiusSmall = 900;
turnRadiusBig = 1400;

%% Level flight for 2500m in altitude 1000m and airspeed 70m/s
horizontal1x = linspace(0, 1000, 2);
horizontal1y = linspace(0, 0, 2);
horizontal1z = linspace(1000, 1000, 2);
horizontal1v = [70, 70];

%% Left turn with a radius of 250 meters without change of altitude or speed
turn1x = cos(deg2rad(-90:10:90)) * turnRadiusSmall + horizontal1x(end);
turn1y = sin(deg2rad(-90:10:90)) * turnRadiusSmall + turnRadiusSmall + horizontal1y(end);
turn1z = ones(size(turn1x)) * horizontal1z(end);
turn1v = ones(size(turn1x)) * horizontal1v(end);

%% Flight without change of heading for 45 km
%  Climb from 1000m to 3500m while increasing airspeed from 70 to 90 m/s
climbx = linspace(turn1x(end), turn1x(end) - 45000, 500);
climby = ones(1, 500) * turn1y(end);
climbz = linspace(turn1z(end), turn1z(end) + 2500, 500);
climbv = linspace(turn1v(end), maxSpeed, 500);

%% Left turn with a radius of 1400 meters without change of altitude or speed
% (at 3500 meters and 90 m/s)
turn2x = cos(deg2rad(90:10:270)) * turnRadiusBig + climbx(end);
turn2y = sin(deg2rad(90:10:270)) * turnRadiusBig - turnRadiusBig + climby(end);
turn2z = ones(size(turn2x)) * climbz(end);
turn2v = ones(size(turn2x)) * maxSpeed;

%% Flight without change of heading for 30 km
% Decent from 3500 to 2500m with an airspeed of 70m/s
decentx = linspace(turn2x(end), turn2x(end) + 30000, 333);
decenty = ones(size(decentx)) * turn2y(end); 
decentz = linspace(turn2z(end), 2500, 333);
decentv = linspace(maxSpeed, 70, 333);

%% Left turn by 2340 degrees (6.5x360) while descending to 500 meters in altitude 
turns = 6.5;
angles = deg2rad(-90:10:(90 + turns * 360));
turnradius = linspace(turnRadiusBig, turnRadiusSmall, length(angles) / 2);
turnradius = [turnradius, linspace(turnRadiusSmall, turnRadiusBig, length(angles) / 2 + 1)];

turn3x = cos(angles) .* turnradius + decentx(end);
turn3y = sin(angles) .* turnradius + turnRadiusBig + decenty(end);
turn3z = linspace(decentz(end), 500, length(turn3x));
turn3v = linspace(decentv(end), 50, length(turn3x));

%% Some final straight movement as 50m/s for 1000m at 500m
horizontal2x = linspace(turn3x(end), turn3x(end) + 1000,2);
horizontal2y = ones(1, 2) * turn3y(end);
horizontal2z = ones(1, 2) * turn3z(end);
horizontal2v = ones(1, 2) * turn3v(end);

%% Put everything together
x = [horizontal1x,turn1x,climbx,turn2x,decentx,turn3x,horizontal2x];
y = [horizontal1y,turn1y,climby,turn2y,decenty,turn3y,horizontal2y];
z = [horizontal1z,turn1z,climbz,turn2z,decentz,turn3z,horizontal2z];
v = [horizontal1v,turn1v,climbv,turn2v,decentv,turn3v,horizontal2v];